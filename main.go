// TinyHTTPProxy
package main

import (
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"net/http/httputil"
	"net/url"
	"os"

	"github.com/buger/jsonparser"
)

type Config struct {
	ListenPort   int64
	DefaultHost  string
	HostMappings map[string]string
}

var config Config

func LoadConfig() Config {
	configuration := "./proxy.conf"
	_, err := os.Stat(configuration)
	if os.IsNotExist(err) {
		fmt.Println("proxy.conf not found. Exiting")
		os.Exit(1)
	}

	f, err := os.Open(configuration)
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
	defer f.Close()

	bytes, err := ioutil.ReadFile(configuration)
	if err != nil {
		fmt.Printf("Failed to read %s\n", configuration)
		os.Exit(1)
	}

	listenPort, err := jsonparser.GetInt(bytes, "ListenPort")
	if err != nil {
		fmt.Printf("Failed to read ListenPort from %s\n", configuration)
		os.Exit(1)
	}
	defaultHost, err := jsonparser.GetString(bytes, "DefaultHost")
	if err != nil {
		fmt.Printf("Failed to read DefaultHost from %s\n", configuration)
		os.Exit(1)
	}

	if defaultHost == "" {
		fmt.Printf("No DefaultHost was specified. Don't know where to proxy pass to. Exiting ..\n")
		os.Exit(1)
	}

	var hostMappings map[string]string = make(map[string]string)

	err = jsonparser.ObjectEach(bytes, func(key []byte, value []byte, dataType jsonparser.ValueType, offset int) error {
		hostMappings[string(key)] = string(value)
		return nil
	}, "HostMappings")

	if err != nil {
		fmt.Printf("Failed to read HostMappings from %s\n", configuration)
	}

	config := Config{
		ListenPort:   listenPort,
		DefaultHost:  defaultHost,
		HostMappings: hostMappings,
	}

	return config
}

type Prox struct {
	target *url.URL
	proxy  *httputil.ReverseProxy
}

var proxies map[string]*Prox
var defaultProxy *Prox

func NewProxy(target string) *Prox {
	url, _ := url.Parse(target)
	return &Prox{target: url, proxy: httputil.NewSingleHostReverseProxy(url)}
}

func (p *Prox) handle(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("X-Proxy-Client-Agent", "TinyHTTPProxy")
	p.proxy.Transport = &myTransport{}
	p.proxy.ServeHTTP(w, r)
}

func main() {
	config := LoadConfig()

	fmt.Printf("Configuration options loaded\n")
	fmt.Printf("ListenPort = %d\n\n", config.ListenPort)

	fmt.Printf("Host proxy map configuration (HOST -> TARGET)\n")
	for key, value := range config.HostMappings {
		fmt.Printf("Proxying requests from %s to %s\n", key, value)
	}

	fmt.Printf("\nDefault proxying host when no mapping found = %s\n", config.DefaultHost)

	// Create proxies for all registered targets including the default
	proxies = make(map[string]*Prox)
	for key, value := range config.HostMappings {
		proxies[key] = NewProxy(value)
	}

	// Add the default route if it wasn't registered above
	var found bool = false
	for value := range config.HostMappings {
		// If the DefaultHost has already been registered above then don't add this
		if value == config.DefaultHost {
			found = true
			break
		}
	}

	if !found {
		defaultProxy = NewProxy(config.DefaultHost)
	}

	//Make a handler that intercepts the host header, finds it in the regisered proxies and palms it off
	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		if r.TLS != nil {
			fmt.Printf("SSL not supported yet\n")
			return
		}

		// TODO detect if there is a colon and port in the host and strip it off
		fmt.Printf("%s\n", r.Host)
		/*
			fmt.Printf("%s\n", r.URL.Host)
			fmt.Printf("%s\n", r.Header.Get("Host"))
			fmt.Printf("%s\n", url.Host)
			fmt.Printf("%s\n", url.Hostname())
			fmt.Printf("%s\n", url.IsAbs())
			fmt.Printf("%s\n", url.RequestURI())
		*/

		// Check if there is a corresponding target to proxy to
		var forwarded bool = false
		for key, value := range config.HostMappings {
			if key == url.Host {
				forwarded = true
				fmt.Printf("Forwarding to %s\n", value)
			}
		}
		if !forwarded {
			// Forward to the default
			fmt.Printf("Forwarded to %s\n", config.DefaultHost)
		}
	})
	log.Fatal(http.ListenAndServe(fmt.Sprintf(":%d", config.ListenPort), nil))

}
